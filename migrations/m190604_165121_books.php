<?php

use yii\db\Migration;

/**
 * Class m190604_165121_books
 */
class m190604_165121_books extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190604_165121_books cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
         $this->createTable('books', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'author_name' => $this->string()->notNull(),
            'desc' => $this->text(),
            'status' => "ENUM('Issue', 'Return', 'Available', 'NotAvailable')",
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down()
    {
        $this->dropTable('news');
    }
    
}
