<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="books-form">

    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

   
    
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
   
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>
    
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'author_name')->textInput(['maxlength' => true, 'placeholder' => 'Author Name']) ?>    
    </div>
    <div class="col-md-6">
     <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>     
    </div>  
    <div class="col-md-6">
        <?= $form->field($model, 'status')->dropDownList([ 'Issue' => 'Issue', 'Return' => 'Return', 'Available' => 'Available', 'NotAvailable' => 'NotAvailable', ], ['prompt' => '']) ?>     
    </div>
    <div class="col-md-12">
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>        
    </div>    


    <?php ActiveForm::end(); ?>

</div>
