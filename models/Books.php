<?php

namespace app\models;

use Yii;
use \app\models\base\Books as BaseBooks;

/**
 * This is the model class for table "books".
 */
class Books extends BaseBooks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'author_name'], 'required'],
            [['desc', 'status'], 'string'],
            [['name', 'author_name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
